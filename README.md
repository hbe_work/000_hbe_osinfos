## Name
000_HBe_OsInfos

## Description
Ce script PowerShell permet de collecter des informations système à partir de l'ordinateur sur lequel il est exécuté. Les données collectées incluent :

    - Nom de l'utilisateur connecté
    - Nom et domaine de l'ordinateur
    - Système d'exploitation et version
    - Adresse IP
    - Ports réseaux établis
    - Capacité et espace libre du disque dur
    - Variables d'environnement système
    - Utilisateurs locaux
    - Heure système
    - Adresse MAC
    - Liste des processus en cours d'exécution
    - Liste des services en cours d'exécution
    - Liste des logiciels installés
    - Contenu du dossier C:\Windows\System32\drivers\etc\

Les données sont stockées dans un fichier XML situé sur le bureau de l'utilisateur courant. Une en-tête de copyright peut être ajoutée à la fin du fichier XML.

## Installation

    1. Assurez-vous que PowerShell est installé sur le poste de travail.

    2. Ouvrez PowerShell en tant qu'administrateur.

    3. Accédez au répertoire où le script est enregistré.

    4. Exécutez le script en tapant la commande suivante :
        .\000_HBe_OsInfos.ps1
        
        ou 

        En faisant clique droit sur le fichier puis "Exécuter avec PowerShell"
    
    5. Les informations collectées seront stockées dans un fichier XML situé sur le bureau de l'utilisateur.

    6. Le fichier XML sera ouvert automatiquement après la fin de l'exécution du script.

Le chemin d'exportation du fichier XML peut être modifié en modifiant la valeur de la variable $exportPath dans le script.

## Support
Si vous avez des difficultés à exécuter le script, une idée d'update ou si vous avez des questions, n'hésitez pas à me contacter pour obtenir de l'aide. Je suis là pour vous aider à résoudre tout problème et à répondre à toutes vos questions.

## Avertissement
Ce script est fourni tel quel, sans garantie d'aucune sorte. L'utilisation de ce script est à vos risques et périls. Assurez-vous de comprendre les conséquences de l'exécution de ce script avant de l'utiliser sur un ordinateur en production.

## Remerciements
Ce script a été écrit avec l'aide de la documentation et de la communauté PowerShell. Les sources utilisées pour développer ce script sont : 

    La documentation officielle de PowerShell : https://docs.microsoft.com/en-us/powershell/
    Le forum PowerShell de Microsoft : https://docs.microsoft.com/en-us/answers/topics/powershell.html
    La communauté PowerShell : https://community.idera.com/database-tools/powershell/

## Authors
Copyright © 2023 Hakim. Tous droits réservés.
