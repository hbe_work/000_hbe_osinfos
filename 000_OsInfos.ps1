####################################################################################################################################################################################################
<#
.SYNOPSIS
000_OsInfos - Recueillir des informations sur votre PC

.DESCRIPTION
Ce script permet de récupérer des informations sur un ordinateur, telles que le nom, le domaine, l'OS, l'adresse IP, les ports réseau et l'espace de stockage C:/ disponible

.PARAMETER ComputerName
Spécifiez un ou plusieurs noms d'ordinateurs pour lesquels vous souhaitez collecter les informations.

.EXAMPLE
Utilisation sur un pc non connus afin de connaitre les informations essentiels sur ce dernier.

.INPUTS
Aucune entrée requise.

.OUTPUTS
Ce script génère un rapport XML contenant les informations sur l'ordinateur.

.NOTES
Auteur : @Hakim
Date de création : 23/02/2023 

Copyright © 2023 Hakim. Tous droits réservés.

Version : 1.0
Dernière modification : NONE
Modification par : NONE

#>
$copyrights = '
Author : @Hakim
Date of creation : 23/02/2023 
Copyright © 2023 Hakim. All rights reserved.
Version : 1.0
Last modification : NONE
Modification by : NONE
'
####################################################################################################################################################################################################>

# Les données sont stockées dans un fichier XML situé dans le répertoire de votre choix
$exportPath = "$env:UserProfile\Desktop\000_Logs_OsInfos.xml"

$results = @(
    # Utilisateur actuellement connecté
    Get-WmiObject Win32_OperatingSystem | Select-Object RegisteredUser
    # Le nom du poste de travail
    Get-WmiObject Win32_OperatingSystem | Select-Object -Property CSName
    # Le domaine du poste de travail
    systeminfo | findstr /B /C:”Domain”
    # OS du poste de travail
    Get-WmiObject Win32_OperatingSystem | Select-Object -Property Caption
    # La version OS du poste de travail
    Get-WmiObject Win32_OperatingSystem | Select-Object -Property Version
    # Adresse IP du poste de travail
    Get-NetIPAddress | Select-Object -Property IPAddress
    # Les Ports réseaux du poste de travail
    Get-NetTCPConnection -State Established
    # La capacité du disque dur du poste de travail (C:)
    Get-CimInstance -Class Win32_LogicalDisk | Select-Object -Property DeviceID, Size
    # Espace libre du poste de travail (C:)
    Get-CimInstance -Class Win32_LogicalDisk | Select-Object -Property DeviceID, FreeSpace
    # Path du poste de travail
    $env:path -split ";"
    # Les utilisateurs locaux du poste de travail
    Get-LocalUser
    # Heure du poste de travail
    Get-Date -Format HH:MM
    # Adresse MAC
    Get-NetAdapter | Select-Object -Property MacAddress
    # Liste des process du poste de travail
    Get-Process
    # Liste des services du poste de travail
    Get-Service
    # Liste des logiciels installés
    Get-WmiObject -Class Win32_Product | Select-Object -Property Name
    # Afficher contenu dossier C:\Windows\System32\drivers\etc\
    Get-ChildItem -Path C:\Windows\System32\drivers\etc\
) 
$results += $copyrights

$results | Export-Clixml $exportPath
Invoke-Item $exportPath